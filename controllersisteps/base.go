package controllersisteps

import (
	model "gitlab.com/AndrusGerman/sisteps-server/modelsisteps"

	"github.com/labstack/echo"
)

// Db database connection
var Db = model.Db

// RouteHello return hello
func RouteHello(c echo.Context) error {
	return c.String(200, "Hello From sisteps server!")
}

// RouteMigrate migrate all Models
func RouteMigrate(c echo.Context) error {
	Db.AutoMigrate(&model.User{})
	return c.String(200,"Finish Migrate")
}

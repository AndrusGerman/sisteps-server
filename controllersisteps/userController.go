package controllersisteps

import (
	"github.com/labstack/echo"
	model "gitlab.com/AndrusGerman/sisteps-server/modelsisteps"
)

// RouteUserCreate create one user
func RouteUserCreate(c echo.Context) error {
	var user = new(model.UserRegister)
	if err := c.Bind(user); err != nil {
		return c.String(200, "Error to bind "+err.Error())

	}
	if err := Db.Create(user.GetUser()).Error; err != nil {
		return c.String(200, "Error to Create "+err.Error())

	}
	return c.JSON(200, user)
}

// RouteUserGetByID get one user
func RouteUserGetByID(c echo.Context) error {
	id := c.Param("id")
	var user = new(model.User)
	if err := Db.Find(user, "id = ?", id).Error; err != nil {
		return c.String(200, "Error to find "+err.Error())
	}
	return c.JSON(200, user)
}

package configsisteps

import (
	"github.com/jinzhu/gorm"
	// Database driver
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// GetDb conections
func GetDb() (*gorm.DB, error) {
	return gorm.Open("sqlite3", "sisteps.db")
}

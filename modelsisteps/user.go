package modelsisteps

import (
	"github.com/jinzhu/gorm"
)

// User Model
type User struct {
	gorm.Model
	Nick     string
	Name     string
	Email    string `json:"-"`
	Password string `json:"-"`
}

// UserRegister model
type UserRegister struct {
	Nick     string
	Name     string
	Email    string
	Password string
}

// GetUser for save in database
func (ctx *UserRegister) GetUser() *User {
	var us = new(User)
	us.Nick = ctx.Nick
	us.Name = ctx.Name
	us.Email = ctx.Email
	us.Password = ctx.Password
	return us
}

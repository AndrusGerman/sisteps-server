package modelsisteps

import (
	cfg "gitlab.com/AndrusGerman/sisteps-server/configsisteps"
)

// Db database connection
var Db, _ = cfg.GetDb()

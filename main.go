package main

import (
	"github.com/labstack/echo"
	"gitlab.com/AndrusGerman/sisteps-server/controllersisteps"
)

func main() {
	e := echo.New()
	api := e.Group("/api")
	{
		// Base
		api.GET("/hello", controllersisteps.RouteHello)
		api.GET("/migrate", controllersisteps.RouteMigrate)
		// User
		api.POST("/user", controllersisteps.RouteUserCreate)
		api.GET("/user/:id", controllersisteps.RouteUserGetByID)
	}
	e.Logger.Fatal(e.Start(":1323"))
}
